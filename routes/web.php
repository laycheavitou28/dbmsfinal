<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [StudentController::class, 'get_all']);

// Grade ROUTE
Route::prefix('grade')->group(function () {
    Route::get("/", [GradeController::class, 'show_all_grade']);
    Route::get("/add", [GradeController::class, 'add_grade']);
    Route::post("/insert", [GradeController::class, 'insert_grade']);
    Route::get("/edit/{id}", [GradeController::class, 'edit_grade']);
    Route::post("/update/{id}", [GradeController::class, 'update_grade']);
    Route::delete("/delete/{id}", [GradeController::class, 'delete_grade']);
});

// Student ROUTE
Route::prefix('student')->group(function () {
    Route::get("/", [StudentController::class, 'show_all_students']);
    Route::get("/add", [StudentController::class, 'add_student']);
    Route::post("/insert", [StudentController::class, 'insert_student']);
    Route::get("/edit/{id}", [StudentController::class, 'edit_student']);
    Route::post("/update/{id}", [StudentController::class, 'update_student']);
    Route::delete("/delete/{id}", [StudentController::class, 'delete_student']);
});
