@extends('layout')

@section('content')
<div class="container">
    <h5 class="mb-3 text-light text-center">Edit Grade</h5>
    <form action="/grade/update/{{$grade->id}}" method="POST">
        @csrf
        <div class="form-group mb-3">
            <label for="gpa" class="text-info">GPA</label>
            <input type="text" class="form-control mt-1 bg-dark text-light border-secondary" id="gpa" name="gpa" value="{{$grade->gpa}}" placeholder="Enter Student GPA">
        </div>
        <div class="form-group mb-3">
            <label for="cgpa" class="text-info">CGPA</label>
            <input type="text" class="form-control mt-1 bg-dark text-light border-secondary" id="cgpa" name="cgpa" value="{{$grade->cgpa}}" placeholder="Enter Student CGPA">
        </div>
        <div class="form-group mb-3">
            <label for="student_name" class="text-info">Student Name</label>
            <input type="text" class="form-control mt-1 bg-dark text-light border-secondary" id="student_name" name="student_name" value="{{$grade->name}}" placeholder="Enter Student Name">
        </div>
        <button type="submit" name="button_save" class="btn btn-info text-light">Submit</button>
    </form>
</div>
@endsection