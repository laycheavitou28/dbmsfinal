@extends('layout')

@section('content')
<div class="container">
    <button type="button" class="btn btn-info"><a href="/grade/add" style="color: white; text-decoration: none">Add New Grade</a></button>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">GPA</th>
                <th scope="col">CGPA</th>
                <th scope="col">Student Name</th>
                <th scope="col">Actions</th>
        </thead>
        <tbody>
            @foreach ($grades as $grade)
            <tr>
                <th scope="row">{{$grade->id}}</th>
                <td>{{$grade->gpa}}</td>
                <td>{{$grade->cgpa}}</td>
                <td>{{$grade->name}}</td>
                <td class="d-flex">
                    <button type="submit" class="btn btn-warning"><a href="/grade/edit/{{$grade->id}}" class="text-white text-decoration-none">Edit</a></button>
                    <div class="mx-1"></div>
                    <form action="/grade/delete/{{$grade->id}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <input name="_method" type="hidden" value="DELETE">
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{--
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete {{$grade->gpa}}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary"><a href="/delete/{{$grade->id}}" style="color: white">Save changes</a></button>
</div>
</div>
</div>
</div> --}}
</div>
@endsection