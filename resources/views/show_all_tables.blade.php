@extends('layout')

@section('content')

<div class="container main ">

    <div class="col">
        <button type="button" class="btn btn-info add-btn"><a href="/student/add" style="color: white; text-decoration: none">Add new student</a></button>

        <table class="table table-dark table-striped table-bordered">
            <caption>List of students</caption>
            <thead class="thead-light border-secondary">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Actions</th>
            </thead>
            <tbody class="border-secondary">
                @foreach ($students as $student)
                <tr>
                    <th scope="row">{{$student->id}}</th>
                    <td style="width: 25%">{{$student->name}}</td>
                    <td class="d-flex">
                        <button type="submit" class="btn btn-secondary small-btn"><a href="/student/edit/{{$student->id}}" class="text-white text-decoration-none">Edit</a></button>
                        <div class="mx-1"></div>
                        <form action="/student/delete/{{$student->id}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input name="_method" type="hidden" value="DELETE">
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col">
        <button type="button" class="btn btn-info add-btn"><a href="/grade/add" style="color: white; text-decoration: none">Add New Grade</a></button>

        <table class="table table-dark table-striped table-bordered">
            <caption>List of students' grades</caption>
            <thead class="thead-light border-secondary">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">GPA</th>
                    <th scope="col">CGPA</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Actions</th>
            </thead>
            <tbody class="border-secondary">
                @foreach ($grades as $grade)
                <tr>
                    <th scope="row">{{$grade->id}}</th>
                    <td>{{$grade->gpa}}</td>
                    <td>{{$grade->cgpa}}</td>
                    <td>{{$grade->name}}</td>
                    <td class="d-flex">
                        <button type="submit" class="btn btn-secondary small-btn"><a href="/grade/edit/{{$grade->id}}" class="text-white text-decoration-none">Edit</a></button>
                        <div class="mx-1"></div>
                        <form action="/grade/delete/{{$grade->id}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input name="_method" type="hidden" value="DELETE">
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{--
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete {{$grade->gpa}}
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary"><a href="/delete/{{$grade->id}}" style="color: white">Save changes</a></button>
</div>
</div>
</div>
</div> --}}
</div>
@endsection

<style>
    .main {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
    }

    .col {
        display: flex;
        flex-direction: column;
        margin-right: 2rem;
    }

    .small-btn {
        width: 4rem;
        height: 2.35rem;
    }

    .add-btn {
        width: fit-content;
        margin-bottom: 1rem;
    }
</style>