<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{

    public function get_all()
    {
        $grades = DB::table('students')
            ->join('grades', 'students.id', '=', 'grades.student_id')
            ->get();
        $students = Student::all();

        return view('show_all_tables', compact('grades', 'students'));
    }

    // display all students
    public function show_all_students()
    {
        $students = DB::table('students')->get();

        return view('student.show_student', compact('students'));
    }

    // display all student form
    public function add_student()
    {
        return view('student.add_student');
    }

    // display edit student form
    public function edit_student($id)
    {
        $student = Student::where('id', '=', $id)->first();

        return view('student.edit_student', compact('student'));
    }

    // add new students to the DB
    public function insert_student(Request $request)
    {
        $student = Student::create([
            'name' => $request->student_name,
            'department' => $request->department,
            'enrollment_year' => $request->enrollment_year,
        ]);

        if ($student) {
            return redirect('/');
        }
    }

    // edit student information
    public function update_student(Request $request, $id)
    {
        $student = Student::where('id', '=', $id)->update([
            'name' => $request->student_name
        ]);

        return redirect('/');
    }

    // delete student from the database
    public function delete_student($id)
    {
        $student = Student::where('id', '=', $id)->delete();

        return redirect('/');
    }
}
