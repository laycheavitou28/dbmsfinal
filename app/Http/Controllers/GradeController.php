<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GradeController extends Controller
{
    // display all grades
    public function show_all_grade()
    {
        $grades = DB::table('students')
            ->join('grades', 'students.id', '=', 'grades.student_id')
            ->get();

        return view('grade.show_grade', compact('grades'));
    }

    // display add grade form
    public function add_grade()
    {
        return view('grade.add_grade');
    }

    // display edit grade form
    public function edit_grade($id)
    {
        $grade = DB::table('students')
            ->join('grades', 'students.id', '=', 'grades.student_id')
            ->where('grades.id', '=', $id)
            ->first();

        return view('grade.edit_grade', compact('grade'));
    }

    // add new grade to the database
    public function insert_grade(Request $request)
    {
        $studentExist = Student::where('name', '=', $request->student_name)->first();

        if ($studentExist == null) {
            $student = Student::create([
                'name' => $request->student_name
            ]);
        }

        $grade = Grade::create([
            'gpa' => $request->gpa,
            'cgpa' => $request->cgpa,
            'student_id' => $studentExist != null ? $studentExist->id : $student->id
        ]);

        if ($grade) {
            return redirect('/');
        }
    }

    // edit grade information
    public function update_grade(Request $request, $id)
    {
        $grade = Grade::where('id', '=', $id)->first();

        $student = Student::where('id', '=', $grade->student_id)->update([
            'name' => $request->student_name
        ]);

        $gradeUpdate = Grade::where('id', '=', $id)->update([
            'gpa' => $request->gpa,
            'cgpa' => $request->cgpa,
        ]);

        return redirect('/');
    }

    // delete grade from the database
    public function delete_grade($id)
    {
        $grade = Grade::where('id', '=', $id)->delete();

        return redirect('/');
    }
}
